package edu.avans.hartigehap.util.impl;

import java.util.ArrayList;

import edu.avans.hartigehap.domain.DiningTable;
import edu.avans.hartigehap.util.CustomIterator;

public class DiningTableIterator implements CustomIterator {
	private ArrayList<DiningTable> diningTables;
	private int index;
	
	public DiningTableIterator(ArrayList<DiningTable> diningTables2) {
		this.diningTables = diningTables2;
	}
	
	@Override
    public boolean hasNext() {
    
       if(index < diningTables.size()){
          return true;
       }
       return false;
    }

	@Override
	public Object next() {
        if(this.hasNext()){
        	DiningTable dt = diningTables.get(index++);
           return dt;
        }
        return null;
     }

}
