package edu.avans.hartigehap.util.impl;


import java.util.ArrayList;
import java.util.Collection;

import edu.avans.hartigehap.domain.DiningTable;
import edu.avans.hartigehap.util.CustomAggregate;
import edu.avans.hartigehap.util.CustomIterator;

public class DiningTableAggregate implements CustomAggregate {
	private CustomIterator concreteIterator;
	private ArrayList<DiningTable> array;
	
	public DiningTableAggregate(Collection<DiningTable> array2) {
		this.array = new ArrayList<DiningTable>(array2);
		concreteIterator = new DiningTableIterator(array);
	}
	
	public CustomIterator iterator() {
		return concreteIterator;
	}
}
