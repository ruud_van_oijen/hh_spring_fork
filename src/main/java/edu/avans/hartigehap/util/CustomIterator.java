package edu.avans.hartigehap.util;

public interface CustomIterator {
	public boolean hasNext();
	public Object next();
}
