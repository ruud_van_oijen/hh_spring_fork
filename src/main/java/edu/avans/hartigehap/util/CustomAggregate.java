package edu.avans.hartigehap.util;

public interface CustomAggregate {
	public CustomIterator iterator();
}
