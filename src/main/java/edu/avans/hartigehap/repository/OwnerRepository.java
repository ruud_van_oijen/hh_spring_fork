package edu.avans.hartigehap.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import java.util.List;
import java.util.Collection;
import org.springframework.data.domain.*;
import edu.avans.hartigehap.domain.*;



public interface OwnerRepository extends PagingAndSortingRepository<Owner, Long> {
	List<Owner> findByRestaurants(Collection<Restaurant> restaurants, Sort sort);
	List<Owner> findByName(String name);
}

