package edu.avans.hartigehap.repository;


import edu.avans.hartigehap.domain.Role;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
//    /Role findByRolename(String authority);
}