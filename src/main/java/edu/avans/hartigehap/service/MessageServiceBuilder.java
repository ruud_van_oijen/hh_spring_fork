package edu.avans.hartigehap.service;

public interface MessageServiceBuilder {
	String[] getSupportedTypes();
	MessagingService getImplementationByName(String name);
}
