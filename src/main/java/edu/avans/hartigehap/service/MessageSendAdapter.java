package edu.avans.hartigehap.service;

import edu.avans.hartigehap.domain.StateMessage;

public interface MessageSendAdapter {
	void Send(StateMessage message);
}
