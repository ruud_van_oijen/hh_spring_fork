package edu.avans.hartigehap.service.impl;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.avans.hartigehap.service.MessageSendAdapter;
import edu.avans.hartigehap.service.MessageServiceBuilder;
import edu.avans.hartigehap.service.MessagingService;

@Service("messageServiceBuilder")

public class MessageServiceBuilderImpl implements MessageServiceBuilder {

	public static final String MAILNAME = "mail";
	public static final String SMSNAME = "sms";
	public static final String TELEGRAMNAME = "telegram";
	
	
	@Override
	public String[] getSupportedTypes() {
		return new String[] { SMSNAME, MAILNAME, TELEGRAMNAME };
	}

	@Override
	public MessagingService getImplementationByName(String name) {
		MessageSendAdapter messageSendAdapter = MessageSendAdapterFactory.getImplementationByName(name);
		
		if(messageSendAdapter == null){
			return null;
		}
		
		switch (name) {
		case SMSNAME:
			return new SMSMessagingService(messageSendAdapter);
		case MAILNAME:
			return new MailMessagingService(messageSendAdapter);
		default:
			return null;
		}
	}

}
