package edu.avans.hartigehap.service.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.messagebird.*;
import com.messagebird.exceptions.GeneralException;
import com.messagebird.exceptions.UnauthorizedException;
import com.messagebird.objects.MessageResponse;

import edu.avans.hartigehap.domain.StateMessage;
import edu.avans.hartigehap.service.MessageSendAdapter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SMSMessageSendAdapterImpl implements MessageSendAdapter {

	@Override
	public void Send(StateMessage message) {
		// TODO Auto-generated method stub
		// First create your service object
		final MessageBirdService wsr = new MessageBirdServiceImpl("3YWwdvzVxabOZWG0gGIt3u07S");

		// Add the service to the client
		final MessageBirdClient messageBirdClient = new MessageBirdClient(wsr);

		try {
			// Get Hlr using msgId and msisdn
			final List<BigInteger> phones = new ArrayList<BigInteger>();
			phones.add(getPhoneNumber(message));

			log.info(String.format("sending state message to: %s", message.getAdress()));
			final MessageResponse response = messageBirdClient.sendMessage("MessageBird", message.getBody(), phones);
			String responseMessage = response.toString();
			log.info(String.format("sending state message to %s, returned %s", message.getAdress(), responseMessage));
		} catch (UnauthorizedException unauthorized) {
			log.error("UnauthorizedException while sending sms", unauthorized);
		} catch (GeneralException generalException) {
			log.error("GeneralException while sending sms", generalException);
		}
	}

	private BigInteger getPhoneNumber(StateMessage message) {
		BigInteger result = new BigInteger(message.getAdress());
		return result;
	}

}
