package edu.avans.hartigehap.service.impl;

import java.util.ArrayList;
import java.util.Iterator;

import edu.avans.hartigehap.domain.Restaurant;

class RestaurantIterator implements Iterator {
	private ArrayList<Restaurant> restaurantlist;
    int index;
    
    public RestaurantIterator(ArrayList<Restaurant> restaurantlist) {
    	this.restaurantlist = restaurantlist;
    }
    
    @Override
    public boolean hasNext() {
    
       if(index < restaurantlist.size()){
          return true;
       }
       return false;
    }

    @Override
    public Object next() {
    
       if(this.hasNext()){
          return restaurantlist.get(index++);
       }
       return null;
    }		
 }
