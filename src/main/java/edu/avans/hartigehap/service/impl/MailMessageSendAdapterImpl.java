package edu.avans.hartigehap.service.impl;

import javax.mail.*;
import javax.mail.internet.*;

import edu.avans.hartigehap.domain.StateMessage;
import edu.avans.hartigehap.service.MessageSendAdapter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MailMessageSendAdapterImpl implements MessageSendAdapter{

	@Override
	public void Send(StateMessage message) {

		final String username = "hartigehapavans@gmail.com";
		final String password = "HHAvans1234";
		
		
		java.util.Properties props = new java.util.Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		// Construct the message
		String to = message.getAdress();
		String from = "noreply@hartigehap.nl";
		String subject = message.getSubject();
		Message msg = new MimeMessage(session);
		try {
		    msg.setFrom(new InternetAddress(from));
		    msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
		    msg.setSubject(subject);
		    msg.setText(message.getBody());

		    // Send the message.
		    Transport.send(msg);
		} catch (MessagingException e) {
		    log.error("MessagingException while sending a mail", e);
		}
		
	}

}
