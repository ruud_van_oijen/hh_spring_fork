package edu.avans.hartigehap.service.impl;

import java.util.ArrayList;
import java.util.List;

import edu.avans.hartigehap.domain.Order;
import edu.avans.hartigehap.domain.StateMessage;
import edu.avans.hartigehap.service.MessageSendAdapter;
import edu.avans.hartigehap.service.MessagingService;

public abstract class MessagingServiceTemplate implements MessagingService {

	private MessageSendAdapter sendAdapter;

	public MessagingServiceTemplate(MessageSendAdapter messageSendAdapter) {
		this.sendAdapter = messageSendAdapter;
	}

	@Override
	public void stateChanged(Order order) {
		String body = getBody(order);
		String subject = getSubject(order);

		StateMessage sendMessage = new StateMessage();
		sendMessage.setSubject(subject);
		sendMessage.setBody(body);
		sendMessage.setAdress(order.getMessageInfo().getContactAdress());

		List<StateMessage> sendList = order.getMessagesSend();

		if (sendList == null) {
			sendList = new ArrayList<StateMessage>();
			order.setMessagesSend(sendList);
		}

		this.sendAdapter.Send(sendMessage);
		sendList.add(sendMessage);
	}
}
