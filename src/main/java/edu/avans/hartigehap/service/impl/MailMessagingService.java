package edu.avans.hartigehap.service.impl;

import edu.avans.hartigehap.domain.Order;
import edu.avans.hartigehap.service.MessageSendAdapter;

public class MailMessagingService extends MessagingServiceTemplate {

	public MailMessagingService(MessageSendAdapter messageSendAdapter) {
		super(messageSendAdapter);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getBody(Order order) {
		StringBuilder builder = new StringBuilder();
		builder.append("Geachte klant, \n");
		builder.append("\n");
		builder.append(String.format("De status van uw bestelling is gewijzigd naar '%s'. \n", order.getMessage()));
		builder.append("\n");
		builder.append(String.format("%s \n", order.getDescription()));
		builder.append("\n");
		builder.append("Mvg, \n");
		builder.append("\n");
		builder.append("De Bediening");
		
		return builder.toString();
	}

	@Override
	public String getSubject(Order order) {
		return "Wijziging van de status van uw bestelling";
	}

}
