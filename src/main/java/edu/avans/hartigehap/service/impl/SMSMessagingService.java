package edu.avans.hartigehap.service.impl;

import edu.avans.hartigehap.domain.Order;
import edu.avans.hartigehap.service.MessageSendAdapter;

public class SMSMessagingService extends MessagingServiceTemplate {

	public SMSMessagingService(MessageSendAdapter messageSendAdapter) {
		super(messageSendAdapter);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getBody(Order order) {
		return String.format("De status van uw bestelling is gewijzigd naar '%s'. %s", order.getMessage(), order.getDescription());
	}

	@Override
	public String getSubject(Order order) {
		return "";
	}

}
