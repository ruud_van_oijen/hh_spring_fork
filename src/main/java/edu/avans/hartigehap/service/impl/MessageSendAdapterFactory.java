package edu.avans.hartigehap.service.impl;

import edu.avans.hartigehap.service.MessageSendAdapter;

public class MessageSendAdapterFactory {
	public static MessageSendAdapter getImplementationByName(String name)
	{
		switch (name) {
		case MessageServiceBuilderImpl.SMSNAME:
			return new SMSMessageSendAdapterImpl();
		case MessageServiceBuilderImpl.MAILNAME:
			return new MailMessageSendAdapterImpl();
		default:
			return null;
		}
	}
}
