package edu.avans.hartigehap.service.impl;

import edu.avans.hartigehap.domain.User;
import edu.avans.hartigehap.repository.UserRepository;
import edu.avans.hartigehap.service.IUserDetailsService;
import lombok.extern.slf4j.Slf4j;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userDetailsService")
@Transactional(readOnly = true)
@Slf4j
public class UserDetailsServiceImpl  implements IUserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if(user == null) {
            log.info("Authentication failed, user not found");
            throw new UsernameNotFoundException(username);
        } else {
        	log.info("Authentication succeeded : User name : {}", user.getUsername());
        }
        Hibernate.initialize(user.getAuthorities());
        return user;
    }
}