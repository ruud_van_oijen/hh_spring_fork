package edu.avans.hartigehap.service;

import edu.avans.hartigehap.domain.Order;

public interface MessagingService {
	void stateChanged(Order order);	
	String getBody(Order order);
	String getSubject(Order order);
	
}
