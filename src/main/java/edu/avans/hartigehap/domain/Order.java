package edu.avans.hartigehap.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * 
 * @author Erco
 */
@Entity
@NamedQuery(name = "Order.findSubmittedOrders", query = "SELECT o FROM Order o "
        + "WHERE o.orderStatus = edu.avans.hartigehap.domain.Order$OrderStatus.SUBMITTED "
        + "AND o.bill.diningTable.restaurant = :restaurant " + "ORDER BY o.submittedTime")
// to prevent collision with MySql reserved keyword
@Table(name = "ORDERS")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@Getter
@Setter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@ToString(callSuper = true, includeFieldNames = true, of = { "orderStatus", "orderItems" })
public class Order extends DomainObject {
	private static final long serialVersionUID = 1L;

	public enum OrderStatus {
		CREATED, SUBMITTED, PLANNED, PREPARED, SERVED
	}

	@Transient
	protected OrderState state;
	protected String orderState;
	protected String message;
	protected String description;
	
	@Enumerated(EnumType.STRING)
	private OrderStatus orderStatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	protected Date submittedTime;

	@Temporal(TemporalType.TIMESTAMP)
	protected Date plannedTime;

	@Temporal(TemporalType.TIMESTAMP)
	protected Date preparedTime;

	@Temporal(TemporalType.TIMESTAMP)
	protected Date servedTime;

	// unidirectional one-to-many relationship.
	@OneToMany(cascade = javax.persistence.CascadeType.ALL)
	private Collection<OrderItem> orderItems = new ArrayList<OrderItem>();

	@ManyToOne()
	private Bill bill;
	
	@ManyToOne(cascade = javax.persistence.CascadeType.ALL)
	private MessageInfo messageInfo = new MessageInfo();
	
	@OneToMany(cascade = javax.persistence.CascadeType.ALL)
	private List<StateMessage> messagesSend = new ArrayList<>();

	public Order() {
		orderStatus = OrderStatus.CREATED;
		state = new OrderStateSubmitted();
	}
	
	/* business logic */

	@Transient
	public boolean isSubmittedOrSuccessiveState() {
		return orderStatus != OrderStatus.CREATED;
	}

	// transient annotation, because methods starting with are recognized by JPA
	// as properties
	@Transient
	public boolean isEmpty() {
		return orderItems.isEmpty();
	}

	public void addOrderItem(MenuItem menuItem) {
		Iterator<OrderItem> orderItemIterator = orderItems.iterator();
		boolean found = false;
		while (orderItemIterator.hasNext() == true) {
			OrderItem orderItem = orderItemIterator.next();
			if (orderItem.getMenuItem().equals(menuItem)) {
				orderItem.incrementQuantity();
				// apply discount
				new Discount(orderItem);
				found = true;
				
			}
		}
		if (!found) {
			OrderItem orderItem = new OrderItem(menuItem, 1, 0);
			// apply discount
			new Discount(orderItem);
			orderItems.add(orderItem);

		}
	}

	public void deleteOrderItem(MenuItem menuItem) {
		Iterator<OrderItem> orderItemIterator = orderItems.iterator();
		boolean found = false;
		while (orderItemIterator.hasNext() == true) {
			OrderItem orderItem = orderItemIterator.next();
			if (orderItem.getMenuItem().equals(menuItem)) {
				found = true;
				if (orderItem.getQuantity() > 1) {
					orderItem.decrementQuantity();
					new Discount(orderItem);
				} else {
					// orderItem.getQuantity() == 1
					orderItemIterator.remove();
				}
			}
		}
		if (!found) {
			// do nothing
		}
	}

	public void submit() throws StateException {
		this.state.submit(this);
	}

	public void plan() throws StateException {
		this.state.plan(this);
	}

	public void prepared() throws StateException {
		this.state.prepare(this);
	}

	public void served() throws StateException {
		this.state.serve(this);
	}

	@Transient
	public double getPrice() {
		double price = 0;
		Iterator<OrderItem> orderItemIterator = orderItems.iterator();
		while (orderItemIterator.hasNext()) {
			price += orderItemIterator.next().getPrice();
		}
		return price;
	}

}
