package edu.avans.hartigehap.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * 
 * @author Erco
 */
@Entity
@Table(name = "ORDERITEMS")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@Getter
@Setter
@ToString(callSuper = true, includeFieldNames = true, of = { "menuItem", "quantity", "price" })
@NoArgsConstructor
public class OrderItem extends DomainObject {
	private static final long serialVersionUID = 1L;

	// unidirectional many-to-one; deliberately no cascade
	@ManyToOne
	private MenuItem menuItem;

	private int quantity = 0;
	 @Column(name="Price", columnDefinition="Decimal(10,2)")
	protected double  price;
	
	 //Code by Arie Visser
	@Transient
	protected Discount discount;
	
	//Code by Arie Visser
	protected String discountName;

	public OrderItem(MenuItem menuItem, int quantity, double price) {
		this.menuItem = menuItem;
		this.quantity = quantity;
		this.price = price;
	}

	/* business logic */

	public void incrementQuantity() {
		this.quantity++;
	}

	public void decrementQuantity() {
		assert quantity > 0 : "quantity cannot be below 0";
		this.quantity--;
	}

	public void price(double price) {
		this.price = price;

	}

	@Transient
	public double getPrice() {
		return (this.price * quantity);
		
	}
}
