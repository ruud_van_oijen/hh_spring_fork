package edu.avans.hartigehap.domain;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class PizzaDiscount extends Discount {

	private static final long serialVersionUID = 1L;
	protected double discount = 3;

	public PizzaDiscount (OrderItem orderitem) {
		
			orderitem.discountName = "Pizza Korting - 3 voor 2";
			orderitem.price = (orderitem.getMenuItem().getPrice() /3) * 2;
		
	}
}
