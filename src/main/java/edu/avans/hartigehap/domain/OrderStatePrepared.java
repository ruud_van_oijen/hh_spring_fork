package edu.avans.hartigehap.domain;

import java.util.Date;

import javax.persistence.MappedSuperclass;

import edu.avans.hartigehap.domain.Order.OrderStatus;

@MappedSuperclass
public class OrderStatePrepared extends OrderState {

	public void prepare(Order order) throws StateException {
		if (order.getOrderStatus() != OrderStatus.PLANNED) {
			throw new StateException(
					"not allowed to change order state to prepared, if it is not in the planned state");
		}
		order.setOrderStatus(OrderStatus.PREPARED);
		order.orderState = "PREPARED";
		order.message = "Order is bereid";
		order.description = "Order is bereid en zal z.s.m. geserveerd worden";
		order.preparedTime = new Date();

	}

}