package edu.avans.hartigehap.domain;

import java.util.Collection;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Iterator;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import edu.avans.hartigehap.util.CustomAggregate;
import edu.avans.hartigehap.util.CustomIterator;
import edu.avans.hartigehap.util.impl.DiningTableAggregate;

/**
 * 
 * @author Erco
 */
@Entity
@Table(name = "RESTAURANTS")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@Getter
@Setter
@ToString(callSuper = true, includeFieldNames = true, of = { "menu", "diningTables", "customers", "owners" })
@NoArgsConstructor
public class Restaurant extends DomainObjectNaturalId {
    private static final long serialVersionUID = 1L;

    private String imageFileName;

    // unidirectional one-to-one
    @OneToOne(cascade = CascadeType.ALL)
    private Menu menu = new Menu();

    private ArrayList<DiningTable> diningArrayList = new ArrayList<>();
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "restaurant")
    private Collection<DiningTable> diningTables = new HashSet<DiningTable>(diningArrayList);
    
    // no cascading
    @ManyToMany(mappedBy = "restaurants")
    private Collection<Customer> customers = new ArrayList<>();
    
	// no cascading
	@ManyToMany(mappedBy="restaurants")
	private Collection<Owner> owners = new ArrayList<Owner>();

    public Restaurant(String name, String imageFileName) {
        super(name);
        this.imageFileName = imageFileName;
    }

    // business methods
    public void warmup() {
    	CustomAggregate diningTablesCol = new DiningTableAggregate(diningTables);
    	CustomIterator diningTableIterator = diningTablesCol.iterator();
        while (diningTableIterator.hasNext()) {
           DiningTable dt = (DiningTable)diningTableIterator.next();
           dt.getId();
        }

        Iterator<MenuItem> mealsIterator = menu.getMeals().iterator();
        while (mealsIterator.hasNext()) {
            MenuItem mi = mealsIterator.next();
            mi.getId();
            Iterator<FoodCategory> fcIterator = mi.getFoodCategories().iterator();
            while (fcIterator.hasNext()) {
                fcIterator.next().getId();
            }
        }

        Iterator<MenuItem> drinksIterator = menu.getDrinks().iterator();
        while (drinksIterator.hasNext()) {
            MenuItem mi = drinksIterator.next();
            mi.getId();
            Iterator<FoodCategory> fcIterator = mi.getFoodCategories().iterator();
            while (fcIterator.hasNext()) {
                fcIterator.next().getId();
            }
        }

        Iterator<FoodCategory> foodCategoryIterator = menu.getFoodCategories().iterator();
        while (foodCategoryIterator.hasNext()) {
            FoodCategory fc = foodCategoryIterator.next();
            Iterator<MenuItem> miIterator = fc.getMenuItems().iterator();
            while (miIterator.hasNext()) {
                miIterator.next().getId();
            }
        }

    }
}
