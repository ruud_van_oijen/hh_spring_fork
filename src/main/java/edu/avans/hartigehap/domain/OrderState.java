package edu.avans.hartigehap.domain;

import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public abstract class OrderState {

	protected OrderState state;

	public void submit(Order order) throws StateException  {
	}

	public void plan(Order order) throws StateException{
	}

	public void prepare(Order order) throws StateException{
	}

	public void serve(Order order) throws StateException{
	}
}
