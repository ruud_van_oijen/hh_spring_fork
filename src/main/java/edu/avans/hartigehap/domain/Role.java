package edu.avans.hartigehap.domain;
import org.springframework.security.core.GrantedAuthority;

import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Setter
@Table(name = "role")
public class Role implements Serializable, GrantedAuthority {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum AuthorityType {
		ROLE_EMPLOYEE, ROLE_CUSTOMER;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    //@Enumerated(EnumType.STRING)
    @Column(name = "rolename")
    private String authority;

    public Role(String authority) {
    	  this.authority = authority;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public void setRoleId(Integer roleId) {
        this.id = roleId;
    }

}