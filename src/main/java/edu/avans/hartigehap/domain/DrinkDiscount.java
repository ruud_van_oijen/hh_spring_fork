package edu.avans.hartigehap.domain;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class DrinkDiscount extends Discount {

	private static final long serialVersionUID = 1L;
	protected double discount = 0.75;

	public DrinkDiscount(OrderItem orderitem) {

		orderitem.discountName = "Bier korting - 4=3";
		orderitem.price = orderitem.getMenuItem().getPrice() * discount;

	}
}
