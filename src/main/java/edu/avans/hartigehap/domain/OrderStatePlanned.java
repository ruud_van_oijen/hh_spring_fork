package edu.avans.hartigehap.domain;

import java.util.Date;

import javax.persistence.MappedSuperclass;

import edu.avans.hartigehap.domain.Order.OrderStatus;

@MappedSuperclass
public class OrderStatePlanned extends OrderState {

	public void plan(Order order) throws StateException {
		if (order.getOrderStatus() != OrderStatus.SUBMITTED) {
			throw new StateException(
					"not allowed to change order state to prepared, if it is not in the planned state");
		}
		order.setOrderStatus(OrderStatus.PLANNED);
		order.orderState = "PLANNED";
		order.message = "Order is gepland";
		order.description = "Order is gepland en zal z.s.m. bereid worden";
		order.plannedTime = new Date();
		
	}
}