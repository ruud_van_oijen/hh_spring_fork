package edu.avans.hartigehap.domain;

import javax.persistence.Entity;

import lombok.*;

@Getter
@Setter
@Entity
public class StateMessage extends DomainObject{
	
	private String subject;
	private String body;
	private String adress;
}
