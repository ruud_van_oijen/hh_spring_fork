package edu.avans.hartigehap.domain;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Iterator;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * 
 * @author Arie
 */
@Entity
@Table(name = "Owner")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id") 
@Getter @Setter
@ToString(callSuper=true, includeFieldNames=true, of= {"name"})
public class Owner extends DomainObject {
	private static final long serialVersionUID = 1L;

	private String name;
	
	@ManyToMany(cascade=javax.persistence.CascadeType.ALL)
	private Collection<Restaurant> restaurants = new ArrayList<Restaurant>();
	
}
