package edu.avans.hartigehap.domain;

import java.util.Date;

import javax.persistence.MappedSuperclass;

import edu.avans.hartigehap.domain.Order.OrderStatus;

@MappedSuperclass
public class OrderStateServed extends OrderState {

	public void serve(Order order) throws StateException {
		if (order.getOrderStatus() != OrderStatus.PREPARED) {

			throw new StateException("not allowed to change order state to served, if it is not in the prepared state");
		}
		order.setOrderStatus(OrderStatus.SERVED);
		order.orderState = "SERVED";
		order.message = "Order is geserveerd";
		order.description = "Order is geserveerd!";
		order.servedTime = new Date();
	}
}