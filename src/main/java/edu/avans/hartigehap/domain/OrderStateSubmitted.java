package edu.avans.hartigehap.domain;

import java.util.Date;

import javax.persistence.MappedSuperclass;

import edu.avans.hartigehap.domain.Order.OrderStatus;

@MappedSuperclass
public class OrderStateSubmitted extends OrderState {


	public void submit(Order order) throws StateException {
		if (order.isEmpty()) {
			throw new StateException("not allowed to submit an empty order");
		}
		
		order.setOrderStatus(OrderStatus.SUBMITTED);
		order.orderState = "SUBMITTED";
		order.message = "Order is doorgevoerd";
		order.description = "Order is doorgevoerd en zal z.s.m. gepland worden";
		order.submittedTime = new Date();
	}

	public void plan(Order order) throws StateException {
		state = new OrderStatePlanned();
		state.plan(order);
	}

	public void prepare(Order order) throws StateException {
		state = new OrderStatePrepared();
		state.prepare(order);
	}

	public void serve(Order order) throws StateException {
		state = new OrderStateServed();
		state.serve(order);
	}
}
