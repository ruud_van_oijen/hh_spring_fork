package edu.avans.hartigehap.domain;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class HappyHourDiscount extends Discount {

	private static final long serialVersionUID = 1L;
	protected double discount = 0.9;

	public HappyHourDiscount(OrderItem orderitem) {
		orderitem.discountName = "Happy Hour - 10% Korting";
		orderitem.price = orderitem.getMenuItem().getPrice() - discount;

	}
}
