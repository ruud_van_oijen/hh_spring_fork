package edu.avans.hartigehap.domain;

import javax.persistence.Entity;

import lombok.*;

@Getter
@Setter
@Entity
public class MessageInfo extends DomainObject {
	public MessageInfo()
	{
		messageType = "mail";
		contactAdress = "mmhuijbe2@student.avans.nl ";
	}
	
	private String messageType;
	private String contactAdress;
}
