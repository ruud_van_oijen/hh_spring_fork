package edu.avans.hartigehap.domain;

import java.time.LocalDateTime;
import javax.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public class Discount extends DomainObject {

	private static final long serialVersionUID = 1L;

	public Discount() {
		//empty for jpa support
	}

	public Discount(OrderItem orderitem) {

		if (orderitem.getMenuItem().getId().equals("pizza") && orderitem.getQuantity() % 3==0) {
			new PizzaDiscount(orderitem);
		} else if (orderitem.getMenuItem().toString().startsWith("Drink") && orderitem.getQuantity() == 4) {
			new DrinkDiscount(orderitem);
		} else if (LocalDateTime.now().isAfter(LocalDateTime.of(2016, 12, 22, 19, 00))
				&& LocalDateTime.now().isBefore(LocalDateTime.of(2016, 12, 22, 20, 00, 00))) {
			new HappyHourDiscount(orderitem);
		}
		else {
			orderitem.price = orderitem.getMenuItem().getPrice();
			orderitem.discountName = "Geen korting";
		}
	}
}
